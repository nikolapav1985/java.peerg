import java.awt.event.*;
import javax.swing.*;

/**
*
* QuestionDialog class
*
* it is used to represent a question
*
*/
class QuestionDialog extends JDialog implements ActionListener{
    public String reply;
    public void actionPerformed(ActionEvent e){
        this.reply=e.getActionCommand();
        dispose();
    }
}
