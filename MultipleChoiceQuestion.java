import java.awt.*;
import javax.swing.*;

/**
*
* MultipleChoiceQuestion class
*
* it is used to represent a multiple choice question
*
*/
class MultipleChoiceQuestion extends Question{
    public MultipleChoiceQuestion(String query,String a,String b,String c,String d,String e, String correct){
        this.query=new QuestionDialog();
        this.query.setLayout(new GridLayout(0,1));
        this.query.add(new JLabel(" "+query+" "), JLabel.CENTER);
        this.addChoice("A",a);
        this.addChoice("B",b);
        this.addChoice("C",c);
        this.addChoice("D",d);
        this.addChoice("E",e);
        this.query.setModal(true); // keep control until reply given
        this.query.pack();
        this.query.setLocationRelativeTo(null);
        this.correct=correct; // correct reply
        this.nCorrect=0;
        this.valid="ABCDE";
    }
    public void addChoice(String name, String label){ // add a choice
        JPanel choice=new JPanel(new BorderLayout());
        JButton button=new JButton(name);
        button.addActionListener(this.query);
        choice.add(button,BorderLayout.WEST);
        choice.add(new JLabel(label+"",JLabel.LEFT),BorderLayout.CENTER);
        this.query.add(choice);
    }
    public String ask(){ // ask question and check valid reply
        this.query.setVisible(true);
        return this.query.reply;
    }
    public void check(){ // check if reply correct
        String reply="";
        reply=this.ask();
        if(reply.equals(this.correct)){
            JOptionPane.showMessageDialog(null, "Correct!");
            this.nCorrect++;
        }else{
            JOptionPane.showMessageDialog(null, "Incorrect. The correct reply is "+this.correct+"!");
        }
    }
}
