/**
*
* Question class
*
* abstract class used to represent questions
*
*/
abstract class Question{
    public QuestionDialog query;
    public String correct;
    public int nCorrect;
    public String valid;
    abstract String ask(); // ask question and check valid reply
    abstract void check(); // check if reply correct
}
