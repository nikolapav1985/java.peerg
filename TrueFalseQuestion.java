import java.awt.*;
import javax.swing.*;

/**
*
* TrueFalseQuestion class
*
* it is used to represent a true false choice question
*
*/
class TrueFalseQuestion extends Question{
    public TrueFalseQuestion(String query, String correct){
        this.query=new QuestionDialog();
        this.query.setLayout(new GridLayout(0,1));
        this.query.add(new JLabel(" "+query+" "), JLabel.CENTER);
        JPanel buttons = new JPanel(); // button panel
        this.addButton(buttons,"TRUE");
        this.addButton(buttons,"FALSE");
        this.query.add(buttons);
        this.query.setModal(true); // keep control until reply given
        this.query.pack();
        this.query.setLocationRelativeTo(null);
        this.correct=correct; // correct reply
        this.valid="TRUE,FALSE,YES,NO,T,F,Y,N"; // options that are valid
    }
    public void addButton(JPanel buttons, String label){ // add button to panel
        JButton button=new JButton(label);
        button.addActionListener(this.query);
        buttons.add(button);
    }
    public String ask(){ // ask question and check valid reply
        this.query.setVisible(true);
        return this.query.reply;
    }
    public void check(){ // check if reply correct
        String reply="";
        reply=this.ask();
        String[] correctArr = this.correct.split(",",-1); // get correct options
        int i=0;
        for(;i<correctArr.length;i++){ // loop correct options
            if(correctArr[i].equals(reply)){ // it is correct
                JOptionPane.showMessageDialog(null, "Correct!");
                this.nCorrect++;
                return;
            }
        }
        JOptionPane.showMessageDialog(null, "Incorrect. The correct reply is "+correctArr[0]+"!");
    }
}
